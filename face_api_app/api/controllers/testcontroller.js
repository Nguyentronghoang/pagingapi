'use strict';
  var respository = require('./respository')
  exports.list_all_tasks = async function(req, res) {    
    try {
      const task = await respository.getList();
       res.json(task);
    } catch (err) {
      res.send(err);
    }
    
  };
  exports.paging_db= async function(req, res){
    var pageNo = parseInt(req.query.pageNo);
    var size =parseInt(req.query.size);
    var response;
    if (pageNo < 0 || pageNo === 0) {
      response = { "error": true, "message": "invalid page number, should start with 1" };
      return res.json(response)
    }
    var skip = size * (pageNo - 1);
    var limit = size;
    try {
      const task = await respository.paging(skip,limit);
       res.json(task);
    } catch (err) {
      res.send(err);
    }
  };
exports.read_a_task = async function(req, res) {
  try {
    var id= req.params.taskId;
    const task = await respository.getByID(id);
     res.json(task);
  } catch (err) {
    res.send(err);
  }
};


exports.create_a_task = async function(req, res) {
 
  try {
    var t = req.body;
    const task = await respository.insert(t);
     res.json(task);
  } catch (err) {
    res.send(err);
  }
};


exports.update_a_task = async function(req, res) {
  try {
    var id=req.params.taskId;
    var t = req.body;
    const task = await respository.update(id,t);
     res.json(task);
  } catch (err) {
    res.send(err);
  }
};

exports.delete_a_task = async function(req, res) {
  try {
    var id= req.params.id;
    const task = await respository.delete(id);
     res.json(task);
  } catch (err) {
    res.send(err);
  }
};
exports.filter = async function(req,res){
try{

const field =req.body.field;
console.log(field);

const task= await respository.orderby(field);
res.json(task);
}catch(err){
  res.send(err);
}
}
// exports.list_all_tasks = function(req, res) {
//   Task.find({}, function(err, task) {
//     if (err)
//       res.send(err);
//     var name = task.map((data)=>{return data.name})
//     res.json({"so ban":task.length,task});
//   })
  
// };
// exports.filter= function(req, res){
//   var pageNo = parseInt(req.query.pageNo);
//   var size =parseInt(req.query.size);
//   var query = {};
//   var response;
//   if (pageNo < 0 || pageNo === 0) {
//     response = { "error": true, "message": "invalid page number, should start with 1" };
//     return res.json(response)
//   }
//   query.skip = size * (pageNo - 1)
//   query.limit = size
//   Task.find({},{},query).sort([["name",-1],["status",1]]).exec(function(err, task) {
//     if (err)
//       res.send(err);
//     var name = task.map((data)=>{return data.name})
//     res.json({"so ban":task.length,task});
//   })
// }
// exports.paging_db= function(req, res){
//   var pageNo = parseInt(req.query.pageNo);
//   var size =parseInt(req.query.size);
//   var query = {};
//   var response;
//   if (pageNo < 0 || pageNo === 0) {
//     response = { "error": true, "message": "invalid page number, should start with 1" };
//     return res.json(response)
//   }
//   query.skip = size * (pageNo - 1)
//   query.limit = size
//   Task.count({},function(err,totalCount) {
//     if(err) {
//       response = {"error" : true,"message" : "Error fetching data"}
//     }
//   // Find some documents
//   Task.find({}, {}, query, function (err, task) {
//     // Mongo command to fetch all data from collection.
//     if (err) {
//       response = { "error": true, "message": "Error fetching data" };
//     } else {
//       var totalPages = Math.ceil(totalCount/size)
//       response = {"total page":totalPages,"message": task };
//     }
//     res.json(response);
//   });
// })
// };

 