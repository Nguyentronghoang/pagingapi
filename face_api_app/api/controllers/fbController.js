'use strict';
const request = require('request-promise');
// Lấy các thông tin về bài viết, req đầu vào là access_token và fieldSet
exports.geInfo = function (req, res) {
  const {access_token,FieldSet } = req.body;
  const options = {
    method: 'GET',
    uri: 'https://graph.facebook.com/me',
    qs: {
      access_token: access_token,
      fields: FieldSet
    }
  };
  request(options)
    .then(fbRes => {
      console.log(fbRes.id);
      const parsedRes = JSON.parse(fbRes); // chuyển dữ liệu trả về sang dạng JSON
      res.json(parsedRes);
      console.log(JSON.parse(fbRes).id);
    });
};
exports.getID = async function (req, res) {
  var list_id = [], i;
  const { message, url, access_token } = req.body;
  var listUrl = url.split(",");
  console.log(url);
  console.log(listUrl.length);
  console.log(listUrl[0]);
  for (i = 0; i < listUrl.length; i++) {
    const postImageOptions = {
      method: 'POST',
      uri: `https://graph.facebook.com/v3.2/me/photos`,
      qs: {
        access_token: access_token,
        message: message,
        url: listUrl[i],
        published : "false",
      }
    };
    let fbRes = await request(postImageOptions);
    const parsedRes = JSON.parse(fbRes);
    console.log(parsedRes.id)
    list_id.push(parsedRes.id);
  }
  console.log("Helo" + list_id);
  res.json(list_id);

}