'use strict';
var mongoose = require('mongoose'),
    Task = mongoose.model('Tasks');
exports.getList = function () {

    return new Promise((resolve, reject) => {
        Task.find({}, function (e, t) {
            if (e)
                reject(e);
            else
                resolve(t);
        })
    });

};
exports.paging = function (skin, limit) {
    return new Promise((resolve, reject) => {
        var query = {};
        query.skin = skin;
        query.limit = limit;
        Task.find({}, {}, query, function (e, t) {
            if (e)
                reject(e);
            else
                resolve(t);
        })
    });

};
exports.getByID = function (id) {

    return new Promise((resolve, reject) => {
        Task.findById(id, function (e, t) {
            if (e)
                reject(e);
            else
                resolve(t);
        })
    });

};
exports.insert = function (task) {

    return new Promise((resolve, reject) => {
        var new_task = new Task(task);
        new_task.save(function (e, t) {
            if (e)
                reject(e);
            else
                resolve(t);
        })
    });

};
exports.update = function (id, task) {

    return new Promise((resolve, reject) => {
        Task.findOneAndUpdate({ _id: id }, task, { new: true }, function (e, t) {
            if (e)
                reject(e);
            else
                resolve(t);
        });
    });

};
exports.delete = function (id) {

    return new Promise((resolve, reject) => {
        Task.remove({
            _id: id
        }, function (e, t) {
            if (e)
                reject(e);
            else
                resolve({ message: 'Task successfully deleted' })

        });
    });

};
exports.orderby = function (field) {
    return new Promise((resolve, reject) => {
        Task.find({}).sort(field).exec(function (e, t) {
            if (e)
                reject(e);
            else
                resolve(t);
        })
    })
}