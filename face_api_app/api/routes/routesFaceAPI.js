const request = require('request-promise');
module.exports = (app) => {
var fbController = require('../controllers/fbController');
  // you'll need to have requested 'user_about_me' permissions
  // in order to get 'quotes' and 'about' fields from search
  
  app.post('/facebook-search', fbController.geInfo);

  app.post('/facebook-Post', (req, res) => {
    const { id, message, user_access_token } = req.body;
    const postTextOptions = {
      method: 'POST',
      uri: `https://graph.facebook.com/v3.2/${id}/feed`,
      qs: {
        access_token: user_access_token,
        message: message
      }
    };
    // // kiem tra viec nhan du lieu tu req
    // console.log(id)
    // console.log("van on");
    // console.log(message);
    request(postTextOptions)
      .then(fbRes => {
        console.log(fbRes);
        const parsedRes = JSON.parse(fbRes);
        res.json(parsedRes);

      })
  });

  app.post('/facebook-postVideo', (req, res) => {
    const { id, caption, url, user_access_token } = req.body;
    const postVideoOptions = {
      method: 'POST',
      uri: `https://graph.facebook.com/v3.2/${id}/videos`,
      qs: {
        access_token: user_access_token,
        description: caption,
        file_url: url
      }
    };
    request(postVideoOptions)
      .then(fbRes => {
        const parseRes = JSON.parse(fbRes);
        res.json(parseRes);
      });
  });
  app.post('/facebook-postImg', (req, res) => {
    const { id, caption, url, user_access_token,timeput } = req.body;
    var listUrl = url.split(",");
    const postImageOptions = {
      method: 'POST',
      uri: `https://graph.facebook.com/v3.2/${id}/photos`,
      qs: {
        access_token: user_access_token,
        caption: caption,
        url: listUrl[1],
        backdated_time: timeput
      }
    };
    request(postImageOptions).then(fbRes => {
      const parseRes = JSON.parse(fbRes);
      res.json(parseRes);
    });
  });
  app.post('/facebook-postmultiphotos',fbController.getID);

}